import React from 'react';
import { render, fireEvent } from './tests/helper'
import App from './App';
describe('App Tests', () => {

  const setup = (props) => {
    return render(<App />);
  };
  it('should render App', () => {
    const { asFragment } = setup();
    expect(asFragment()).toMatchSnapshot();
  });


  it('should see all current loans', () => {
    const { getByTestId, getByText } = setup();
    expect(getByTestId('list-container').children.length).toBe(3);
    expect(getByText('Current Loans')).toBeInTheDocument();
  });


  it('should see the total amount available for investments', () => {
    const { getByTestId } = setup();
    expect(getByTestId('total-amounts')).toHaveTextContent("Total amount available for investments: £55,723");
  });


  it('should show the investment modal when user clicks on one', () => {
    const { getAllByText, getByTestId } = setup();
    const investBtn = getAllByText('Invest')[0];
    fireEvent.click(investBtn);
    expect(getByTestId('invest-form')).toBeVisible();
  });

  it('Should allow user can enter a numeric value (invested amount) in the input.', () => {
    const { getAllByText, getByTestId } = setup();
    const investBtn = getAllByText('Invest')[0];
    fireEvent.click(investBtn);
    const investForm = getByTestId('invest-form');
    expect(investForm).toBeVisible();
    fireEvent.change(getByTestId('invest-form-input'), { target: { value: "10"}});
  });


  it('Should allow user to open/close the modal', () => {
    const { getAllByText, getByTestId } = setup();
    const investBtn = getAllByText('Invest')[0];
    const modal = getByTestId('modal');
    expect(modal).toHaveClass('hideModal');
    fireEvent.click(investBtn);
    expect(modal).toHaveClass('showModal');
    fireEvent.click(getByTestId('modal-close-btn'));
    expect(modal).toHaveClass('hideModal');
  });

  it('Should allow user to click the button labelled “Invest”.', () => {
    const { getAllByText, getByTestId } = setup();
    const investBtn = getAllByText('Invest')[0];
    fireEvent.click(investBtn);
    const investForm = getByTestId('invest-form');
    const modal = getByTestId('modal');
    expect(investForm).toBeVisible();
    expect(modal).toBeVisible();

    // total amount
    expect(getByTestId('total-amounts')).toHaveTextContent("Total amount available for investments: £55,723");


    fireEvent.change(getByTestId('invest-form-input'), { target: { value: "10"}});
    fireEvent.submit(investForm);
    expect(modal).toHaveClass('hideModal');

    // highlighted as invested.
    expect(getByTestId('list-container').children[0]).toHaveTextContent('Invested');

    //total amount to update
    expect(getByTestId('total-amounts')).toHaveTextContent("Total amount available for investments: £55,713");

  });

});
