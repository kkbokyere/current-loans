import * as React from "react";
import {Provider} from 'react-redux'
import {render as rtlRender } from '@testing-library/react'
import configureStore from "../state/store";
import {initialState} from "../state/ducks/loans";

const initialReducerState = initialState;


function render(
    ui,
    {
        initialState = initialReducerState,
        ...renderOptions
    } = {},
) {
    function Wrapper({children}) {
        return (
            <Provider store={configureStore(initialState)}>
                {children}
            </Provider>
        )
    }
    return rtlRender(ui, {wrapper: Wrapper, ...renderOptions})
}

// re-export everything
export * from '@testing-library/react'

// override render method
export { render }
