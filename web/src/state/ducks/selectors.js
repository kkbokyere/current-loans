import { createSelector } from 'reselect'
const loansAmountDataSelector = state => state.loans.data.map((loan) => parseFloat(loan.available.replace(/,/g, '')));
export const totalAmountSelector = createSelector(loansAmountDataSelector, (loans) => loans.reduce((acc, curr) => acc + curr, 0));
