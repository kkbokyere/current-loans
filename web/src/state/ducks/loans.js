import { loans } from '../../data/currentLoans';
export const UPDATE_LOAN = "UPDATE_LOAN";
export const SELECT_LOAN = "SELECT_LOAN";

export const initialState = {
    selected: '',
    totalAmount: 0,
    data: loans
};

export function updateLoan(payload) {
    return (dispatch, getState) => {
        dispatch({
            type: UPDATE_LOAN,
            payload: {
                id: getState().loans.selected,
                amount: payload
            }
        })
    }
}

export function selectLoan(payload) {
    return {
        type: SELECT_LOAN,
        payload
    }
}


export default (state = initialState, action) => {
    let { payload } = action;
    switch (action.type) {
        case UPDATE_LOAN:
            return {
                ...state,
                data: updateLoanAmount(state, payload)
            };
            case SELECT_LOAN:
            return {
                ...state,
                selected: payload
            };
        default:
            return state
    }
}

const updateLoanAmount = (state, payload) => {
    return state.data.map((loan) => {
        if(loan.id === payload.id) {
            const oldAmount = parseFloat(loan.amount.replace(/,/g, ''));
            const oldAvailable = parseFloat(loan.available.replace(/,/g, ''));
            const investedAmount = parseFloat(payload.amount.replace(/,/g, ''));
            const totalAmount = oldAmount - investedAmount;
            const available = oldAvailable - investedAmount;
            return {...loan, amount: totalAmount.toLocaleString(), available: available.toLocaleString(), isInvested: true }
        }
        return loan
    })
};
