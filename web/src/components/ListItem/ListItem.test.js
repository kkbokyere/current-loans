import React from 'react';
import { render } from '../../tests/helper'

import ListItem from '../../components/ListItem';
import {loans} from "../../data/currentLoans";
describe('ListItem Tests', () => {


  const setup = (props) => {
    return render(<ListItem {...props}/>);
  };
  it('should render ListItem', async () => {
    const { asFragment } = setup({
      data: {}
    });

    expect(asFragment()).toMatchSnapshot();
  });

  it('should render ListItem with data', async () => {
    const { asFragment, getByText } = setup({
      data: loans[0]
    });
    expect(getByText(loans[0].title)).toBeInTheDocument();
    expect(asFragment()).toMatchSnapshot();
  });
});
