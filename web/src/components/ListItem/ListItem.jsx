import React from 'react';
import PropTypes from 'prop-types';
import styles from './ListItem.module.scss'
import Button from "../Button";

const ListItem = ({ data, handleOnClick }) => {
    return (
        <div className={styles.listItem}>
            <div className={styles.listItemHeader}>
                <h3 className={styles.listItemTitle}>{data.title}</h3>
                <span data-testid="list-item-invested" className={styles.listItemInvested}>{data.isInvested && 'Invested'}</span>
            </div>
            <div className={styles.listItemContent}>
                <div className={styles.listItemDetails}>{Object.keys(data).map((key) => <span className={styles.listItemDetailProp} key={key}>{key} : {data[key]}</span>)}</div>
            <Button handleOnClick={handleOnClick}>Invest</Button>
            </div>
        </div>
    );
};

ListItem.propTypes = {
    data: PropTypes.object,
    handleOnClick: PropTypes.func,
};

export default React.memo(ListItem);
