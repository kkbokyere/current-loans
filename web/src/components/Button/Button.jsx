import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.scss'

const Button = ({ children, handleOnClick, ...props }) => {
    return (
        <button onClick={handleOnClick} className={styles.button} {...props}>
            {children}
        </button>
    );
};

Button.propTypes = {
    children: PropTypes.any,
    handleOnClick: PropTypes.func,
};

export default Button;
