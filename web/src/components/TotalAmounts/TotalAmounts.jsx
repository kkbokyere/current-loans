import React from 'react';
import PropTypes from 'prop-types'

const TotalAmounts = ({ total = 0 }) => {
    return (
        <p data-testid="total-amounts">Total amount available for investments: <b>£{total.toLocaleString()}</b></p>
    );
};

TotalAmounts.propTypes = {
    total: PropTypes.number
};

export default TotalAmounts;
