import {render} from "../../tests/helper";
import React from "react";
import TotalAmounts from "./TotalAmounts";

describe('TotalAmounts Tests', () => {
    const setup = (props) => {
        return render(<TotalAmounts {...props} />);
    };
    it('should render TotalAmounts', () => {
        const { asFragment } = setup();
        expect(asFragment()).toMatchSnapshot();
    });

    it('should render App', () => {
        const { getByTestId } = setup({
            total: 1000
        });
        expect(getByTestId("total-amounts")).toHaveTextContent("Total amount available for investments: £1,000");
    });
});
