import React, {Fragment} from "react";
import List from "../List";
import TotalAmounts from "../TotalAmounts";

const LoansList = ({ data, totalAmount, handleOnClickInvest }) => {
    return (
        <Fragment>
            <List data={data} handleOnClickInvest={handleOnClickInvest}/>
            <TotalAmounts total={totalAmount}/>
        </Fragment>

    )
};

export default LoansList;
