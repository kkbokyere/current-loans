import React from "react";
import cx from 'classnames';
import PropTypes from 'prop-types';
import styles from './Modal.module.scss'

const Modal = ({ handleClose, show, children }) => {
    const modalClass = cx(styles.modal,{
        [styles.showModal]: show,
        [styles.hideModal]: !show
    });
    return (
        <div className={modalClass} data-testid="modal">

            <div className={styles.modalContent}>
                <div className={styles.modalHeader}>
                    <button onClick={handleClose} data-testid="modal-close-btn">close</button>
                </div>

                {children}
            </div>
        </div>
    );
};
Modal.propTypes = {
    show: PropTypes.bool,
    children: PropTypes.any,
    handleClose: PropTypes.func,
};
export default Modal;
