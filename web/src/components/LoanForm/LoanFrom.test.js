import React from 'react';
import { render, fireEvent } from '../../tests/helper'

import LoanForm from '../../components/LoanForm';
describe('LoanForm Tests', () => {

    const setup = (props) => {
        return render(<LoanForm data={{
            "id": "1",
            "title": "Voluptate et sed tempora qui quisquam.",
            "tranche": "A",
            "available": "11,959",
            "annualised_return": "8.60",
            "term_remaining": "864000",
            "ltv": "48.80",
            "amount": "85,754"
        }} {...props}/>);
    };
    it('should render LoanForm', async () => {
        const { asFragment } = setup();
        expect(asFragment()).toMatchSnapshot();
    });

    it('should call handleUpdateLoan function when submitting the form', async () => {
        const handleUpdateLoan = jest.fn();
        const { getByTestId } = setup({
            handleUpdateLoan
        });
        fireEvent.submit(getByTestId('invest-form'));
        expect(handleUpdateLoan).toHaveBeenCalled();
    });

    it('should call handleUpdateLoan function with search when clicking the invest button', async () => {
        const handleUpdateLoan = jest.fn();
        const { getByTestId } = setup({
            handleUpdateLoan
        });
        const inputEl = getByTestId('invest-form-input');
        fireEvent.change(inputEl, { target: { value: "10"}});
        fireEvent.click(getByTestId('invest-form-btn'));
        expect(handleUpdateLoan).toHaveBeenCalledWith("10");
    });

});
