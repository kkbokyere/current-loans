import React, {useState} from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import styles from './LoanForm.module.scss'
import Button from "../Button";

const LoanForm = ({ data, handleUpdateLoan }) => {
    const [amount, setAmount] = useState("0");
    if(!data) {
        return null
    }
    const handleOnChangeAmount = (e) => {

        setAmount(e.currentTarget.value)
    };
    const handleOnSubmit = (e) => {
        setAmount('');
        handleUpdateLoan(amount);
        e.preventDefault();
    };
    const loanEnd = moment().add(data.term_remaining, 'minutes');
    return (
        <form onSubmit={handleOnSubmit} data-testid="invest-form">
            <h3>Invest in a Loan</h3>
            <p>{data.title}</p>
            <p>Amount available: £{data.available.toLocaleString()}</p>
            <p>Loan Ends: {loanEnd.fromNow()}</p>
            <p>Investment amount (£)</p>
            <div className={styles.formContent}>
                <input value={amount} type="text" className={styles.formInput} onChange={handleOnChangeAmount} data-testid="invest-form-input"/>
                <Button handleOnClick={handleOnSubmit} data-testid="invest-form-btn">Invest</Button>
            </div>
        </form>
    );
};

LoanForm.propTypes = {
    data: PropTypes.object,
    handleOnClick: PropTypes.func,
};

export default LoanForm;
