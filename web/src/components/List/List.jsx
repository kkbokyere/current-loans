import React from 'react';
import PropTypes from 'prop-types'

import styles from './List.module.scss'
import ListItem from "../ListItem";

const List = ({ data = [], handleOnClickInvest = () => {}}) => {
    return (
        <div className={styles.listContainer} data-testid="list-container">
            {data && data.map(item => <ListItem key={item.id} data={item} handleOnClick={() => handleOnClickInvest(item.id)}/>)}
        </div>
    );
};

List.propTypes = {
    data: PropTypes.array,
    handleOnClickInvest: PropTypes.func,
};

export default React.memo(List);
