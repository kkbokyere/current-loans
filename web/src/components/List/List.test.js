import React from 'react';
import { render } from '../../tests/helper'

import List from '../../components/List';
import {loans} from "../../data/currentLoans";
describe('List Tests', () => {

  const setup = (props) => {
    return render(<List {...props}/>);
  };
  it('should render List', async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });

  it('should render List with 1 items', async () => {
    const { asFragment, getByTestId } = setup({
      data: loans
    });
    expect(getByTestId('list-container').children.length).toBe(3);
    expect(asFragment()).toMatchSnapshot();
  });

  it('should render List with multiple items', async () => {
    const { asFragment, getByTestId } = setup({
      data: loans
    });
    expect(getByTestId('list-container').children.length).toBe(3);
    expect(asFragment()).toMatchSnapshot();
  });

  it('should render List with multiple items', async () => {
    const { asFragment, getByTestId } = setup({
      data: loans,
    });
    expect(getByTestId('list-container').children.length).toBe(3);
    expect(asFragment()).toMatchSnapshot();
  });
});
