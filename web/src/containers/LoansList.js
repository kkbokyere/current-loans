import { connect } from 'react-redux'
import {selectLoan} from "../state/ducks/loans";
import {totalAmountSelector} from "../state/ducks/selectors";
import LoansList from "../components/LoansList";
const mapStateToProps = (state, ownProps) => {
    return {
        data: state.loans.data,
        totalAmount: totalAmountSelector(state)
    }
};

const mapDispatchToProps = (dispatch, { handleOpenModal }) => {
    return {
        handleOnClickInvest: (loanId) => {
            dispatch(selectLoan(loanId));
            handleOpenModal()
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoansList)
