import { connect } from 'react-redux'
import {updateLoan} from "../state/ducks/loans";
import LoanForm from "../components/LoanForm";
const mapStateToProps = (state, ownProps) => {
    return {
        data: state.loans.data.find(({ id }) => id === state.loans.selected),
    }
};

const mapDispatchToProps = (dispatch, { handleCloseModal }) => {
    return {
        handleUpdateLoan: (amount) => {
            dispatch(updateLoan(amount));
            handleCloseModal()
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoanForm)
