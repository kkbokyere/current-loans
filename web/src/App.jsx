import React, {useState} from 'react';
import './styles/App.scss';
import Layout from "./components/Layout";
import LoansList from "./containers/LoansList";
import Modal from "./components/Modal/Modal";
import LoanForm from "./containers/LoanForm";

function App() {
    const [showModal, setShowModal] = useState(false);
    const handleOpenModel = () => {
        setShowModal(true)
    };

    const handleCloseModel = () => {
        setShowModal(false)
    };
  return (
    <div className="App">
      <Layout>
        <h1>Current Loans</h1>
        <LoansList handleOpenModal={handleOpenModel}/>
          <Modal show={showModal} handleClose={handleCloseModel}>
              <LoanForm handleCloseModal={handleCloseModel}/>
          </Modal>
      </Layout>
    </div>
  );
}

export default App;
